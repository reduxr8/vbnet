﻿Public Class Form1
    Public ShopItems As ArrayList = New ArrayList()

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim result As DialogResult = OpenFileDialog1.ShowDialog()

        If result = Windows.Forms.DialogResult.OK Then
            TextBox3.Text = OpenFileDialog1.FileName
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        ShopItems.Add(New ShopItem(TextBox1.Text, TextBox2.Text, TextBox3.Text))
        Dim CurrentItem = ShopItems(ShopItems.Count - 1)
        TextBox1.ResetText()
        TextBox2.ResetText()
        TextBox3.ResetText()
        ListBox1.Items.Add(String.Format("{0} - {1}", CurrentItem.name, CurrentItem.price))
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        ListBox1.Items.Remove(ListBox1.SelectedItem)
        ShopItems.RemoveAt(ShopItems.Count - 1)
    End Sub
End Class
