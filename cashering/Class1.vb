﻿Public Class ShopItem
    Public name As String
    Public price As Double
    Public picpath As String

    Public Sub New(ByVal name, ByVal price, ByVal picpath)
        Me.name = name
        Me.price = price
        Me.picpath = picpath
    End Sub
End Class